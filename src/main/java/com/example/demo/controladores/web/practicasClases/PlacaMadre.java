package com.example.demo.controladores.web.practicasClases;

public class PlacaMadre {
    private String socket;
    private String marca;
    private String modelo;
    private int cantidadBancosMemorias;
    private String tipoMemoria;
    private int cantidadBancosPCIExpress;

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCantidadBancosMemorias() {
        return cantidadBancosMemorias;
    }

    public void setCantidadBancosMemorias(int cantidadBancosMemorias) {
        this.cantidadBancosMemorias = cantidadBancosMemorias;
    }

    public String getTipoMemoria() {
        return tipoMemoria;
    }

    public void setTipoMemoria(String tipoMemoria) {
        this.tipoMemoria = tipoMemoria;
    }

    public int getCantidadBancosPCIExpress() {
        return cantidadBancosPCIExpress;
    }

    public void setCantidadBancosPCIExpress(int cantidadBancosPCIExpress) {
        this.cantidadBancosPCIExpress = cantidadBancosPCIExpress;
    }
}
