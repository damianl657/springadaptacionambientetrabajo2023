package com.example.demo.controladores.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/java")
public class PracticaJava {
    @GetMapping("/practica1")
    public String practica1(Model modelo){
        System.out.println("-------------------");

        System.out.println("Hola Mundo");


        return "practicajava/practica1";
    }
    @GetMapping("/practicaAlumno")
    public String practicaAlumno(Model modelo){
        //no borrar este renglon. ES UN SEPARADOR PARA LA CONSOLA
        System.out.println("-------------------");
        System.out.println("Hola Mundo");


        return "practicajava/practica1";
    }
}
