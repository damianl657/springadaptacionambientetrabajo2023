package com.example.demo.controladores.web;


import com.example.demo.controladores.web.practicasClases.PlacaMadre;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping("/thymeleaf")
public class PracticaThymeleaf {
    @GetMapping("/practicaProfe1")
    public String practicaProfe1(Model modelo){
        // mostrar componentes de bootstrap
        return "practicathymeleaf/practicaProfe1";
    }
    @GetMapping("/practicaAlumnos1")
    public String practicaAlumnos1(Model modelo){
        // GENERAR FORMULARIOS
        //TABLAS
        //BARRA DE MENU
        return "practicathymeleaf/practicaAlumnos1";
    }
    @GetMapping("/practicaAlumnos2")
    public String practicaAlumnos2(Model modelo){
        // GENERAR UNA BARRA DE MENU
        //UN FORMULARIO DE CARGA DE UNA PLACA MADRE
        //UNA LISTA AL FINAL
        return "practicathymeleaf/practicaAlumnos2";
    }

    // Data Binding
    @GetMapping("/practicaProfe2")
    public String practicaProfe2(Model modelo){
        // Data Binding primera parte
        String titulo ="Titulo del sitio web";
        String parrafo = "este es un parrafo";
        modelo.addAttribute("titulo",titulo);
        modelo.addAttribute("parrafo",parrafo);


        return "practicathymeleaf/practicaProfe2";
    }

    @GetMapping("/practicaProfe3")
    public String practicaProfe3(Model modelo){
        // Data Binding de objetos
        PlacaMadre placaMadre = new PlacaMadre();
        placaMadre.setMarca("Gigbyte");
        placaMadre.setCantidadBancosMemorias(4);
        placaMadre.setModelo("Z690");
        placaMadre.setCantidadBancosPCIExpress(1);
        placaMadre.setSocket("1700");
        placaMadre.setTipoMemoria("DDR5");
        modelo.addAttribute("placaMadre",placaMadre);
        String descripcion = "Descripcion del Hardware";
        modelo.addAttribute("descripcion",descripcion);
        return "practicathymeleaf/practicaProfe3";
    }
    @GetMapping("/practicaProfe4")
    public String practicaProfe4(Model modelo){
        // Data Binding de objetos
        PlacaMadre placaMadre = new PlacaMadre();
        placaMadre.setMarca("Gigbyte");
        placaMadre.setCantidadBancosMemorias(4);
        placaMadre.setModelo("Z690");
        placaMadre.setCantidadBancosPCIExpress(1);
        placaMadre.setSocket("1700");
        placaMadre.setTipoMemoria("DDR5");

        PlacaMadre placaMadre2 = new PlacaMadre();
        placaMadre2.setMarca("MSI");
        placaMadre2.setCantidadBancosMemorias(4);
        placaMadre2.setModelo("Mag B660m");
        placaMadre2.setCantidadBancosPCIExpress(1);
        placaMadre2.setSocket("1700");
        placaMadre2.setTipoMemoria("DDR5");

        PlacaMadre placaMadre3 = new PlacaMadre();
        placaMadre3.setMarca("Asus");
        placaMadre3.setCantidadBancosMemorias(4);
        placaMadre3.setModelo("Z690-P");
        placaMadre3.setCantidadBancosPCIExpress(1);
        placaMadre3.setSocket("1700");
        placaMadre3.setTipoMemoria("DDR5");

        List<PlacaMadre> placaMadreList = new LinkedList();
        placaMadreList.add(placaMadre);
        placaMadreList.add(placaMadre2);
        placaMadreList.add(placaMadre3);
        String descripcion = "Lista de Placas Madres";
        modelo.addAttribute("descripcion",descripcion);
        modelo.addAttribute("placaMadreList",placaMadreList);
        return "practicathymeleaf/practicaProfe4";
    }

    @GetMapping("/practicaProfe5")
    public String practicaProfe5(Model modelo){
        // Data Binding de objetos
        PlacaMadre placaMadre = new PlacaMadre();

        modelo.addAttribute("placaMadre",placaMadre);
        String descripcion = "Registrar Placa Madre";
        modelo.addAttribute("descripcion",descripcion);
        return "practicathymeleaf/practicaProfe5";
    }
    @PostMapping("/guardarpracticaProfe5")
    public String guardarpracticaProfe5(@ModelAttribute("placaMadre") PlacaMadre placaMadre, Model modelo){
        System.out.println("Datos Placa Madre:");
        System.out.println("Marca: "+placaMadre.getMarca());
        System.out.println("Modelo: "+placaMadre.getModelo());
        System.out.println("Socket: "+placaMadre.getSocket());
        System.out.println("Tipo de Memoria: "+placaMadre.getTipoMemoria());

        String descripcion = "Registro de Hardware";
        modelo.addAttribute("descripcion",descripcion);
        modelo.addAttribute("placaMadre",placaMadre);
        return "practicathymeleaf/guardarpracticaProfe5";
    }
}

