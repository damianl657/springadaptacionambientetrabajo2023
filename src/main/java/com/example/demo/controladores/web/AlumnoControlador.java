package com.example.demo.controladores.web;

import com.example.demo.modelo.AlumnoModelo;
import com.example.demo.modelo.DivisionModelo;
import com.example.demo.repositorios.AlumnoRepositorio;
import com.example.demo.servicios.AlumnoService;
import com.example.demo.servicios.DivisionServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/alumnos")
public class AlumnoControlador {

    @Autowired
    AlumnoRepositorio alumnoRepositorio;
    @Autowired
    AlumnoService alumnoService;
    @Autowired
    DivisionServicio divisionServicio;

    @GetMapping({"","/"})
    public String listarAlumnos(Model modelo){
        modelo.addAttribute("alumnos",alumnoService.listarTodosActivos());
        return "alumno/alumnos";
    }
    @GetMapping("/nuevo")
    public String crearAlumnoFormulario(Model modelo){
        AlumnoModelo alumnoModelo = new AlumnoModelo();
        List<DivisionModelo> divisionModeloList = divisionServicio.listarTodos();

        modelo.addAttribute("alumno",alumnoModelo);
        modelo.addAttribute("divisiones",divisionModeloList);

        return "alumno/crearAlumnoFormulario";
    }
    @PostMapping("/guardar")
    public String altaAlumno(@ModelAttribute("alumno") AlumnoModelo alumno){
        alumnoService.guardarAlumno(alumno);
        return "redirect:/alumnos";
    }
    @GetMapping("/editar/{id}")
    public String editarAlumnoFormulario(@PathVariable Long id, Model modelo){
        AlumnoModelo alumnoModelo = new AlumnoModelo();
        List<DivisionModelo> divisionModeloList = divisionServicio.listarTodos();
        modelo.addAttribute("alumno",alumnoService.obtenerAlumnoPorId(id));
        modelo.addAttribute("divisiones",divisionModeloList);
        return "alumno/editarAlumnoFormulario";
    }
    @PostMapping("/actualizar/{id}")
    public String actualizarAlumno(@PathVariable Long id, @ModelAttribute("alumno") AlumnoModelo alumno){

        alumnoService.actualizarAlumno(id, alumno);
        return "redirect:/alumnos";
    }
    @GetMapping("/eliminar/{id}")
    public String eliminarAlumno(@PathVariable Long id){
        alumnoService.eliminarAlumno(id);
        return "redirect:/alumnos";
    }

}
