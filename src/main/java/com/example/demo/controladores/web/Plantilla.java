package com.example.demo.controladores.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/plantilla")
public class Plantilla {

    @GetMapping({"","/"})
    public String principal(Model modelo){

        return "plantilla/cuerpo";
    }
}
