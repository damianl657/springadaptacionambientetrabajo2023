package com.example.demo.repositorios;

import com.example.demo.modelo.AlumnoModelo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlumnoRepositorio extends JpaRepository <AlumnoModelo, Long>{
    List<AlumnoModelo> findByEstado(Long estado);
}
