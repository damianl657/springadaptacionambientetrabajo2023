package com.example.demo.repositorios;

import com.example.demo.modelo.DivisionModelo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DivisionRepositorio extends JpaRepository<DivisionModelo,Long> {
}
