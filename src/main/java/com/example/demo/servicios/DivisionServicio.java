package com.example.demo.servicios;

import com.example.demo.modelo.AlumnoModelo;
import com.example.demo.modelo.DivisionModelo;
import com.example.demo.repositorios.DivisionRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DivisionServicio {
    @Autowired
    DivisionRepositorio divisionRepositorio;

    public List<DivisionModelo> listarTodos(){
        return divisionRepositorio.findAll();
    }

    public DivisionModelo obtenerDivisionPorId(Long id){
        return divisionRepositorio.findById(id).get();
    }
}
