package com.example.demo.servicios;

import com.example.demo.modelo.AlumnoModelo;
import com.example.demo.repositorios.AlumnoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlumnoService {

    @Autowired
    private AlumnoRepositorio alumnoRepositorio;

    public List<AlumnoModelo> listarTodos(){
        return alumnoRepositorio.findAll();
    }
    public List<AlumnoModelo> listarTodosActivos(){
        return alumnoRepositorio.findByEstado(1L);
    }
    public List<AlumnoModelo> listarTodosInactivos(){
        return alumnoRepositorio.findByEstado(0L);
    }
    public AlumnoModelo guardarAlumno(AlumnoModelo alumno){
        alumno.setEstado(1L);
        return alumnoRepositorio.save(alumno);
    }
    public AlumnoModelo obtenerAlumnoPorId(Long id){
        return alumnoRepositorio.findById(id).get();
    }
    public AlumnoModelo actualizarAlumno(Long id, AlumnoModelo alumno){

        AlumnoModelo alumnoModeloExistente = alumnoRepositorio.findById(id).get();
        alumnoModeloExistente.setApellido(alumno.getApellido());
        alumnoModeloExistente.setDni(alumno.getDni());
        alumnoModeloExistente.setEmail(alumno.getEmail());
        alumnoModeloExistente.setNombre(alumno.getNombre());
        alumnoModeloExistente.setMatricula(alumno.getMatricula());
        alumnoModeloExistente.setDivision(alumno.getDivision());
        alumnoModeloExistente.setDivisionModelo(alumno.getDivisionModelo());
        alumnoModeloExistente.setGenero(alumno.getGenero());
        alumnoModeloExistente.setEstado(alumno.getEstado());
        return alumnoRepositorio.save(alumnoModeloExistente);
    }
    public AlumnoModelo eliminarAlumno(Long id){
        AlumnoModelo alumnoModeloExistente = alumnoRepositorio.findById(id).get();
        alumnoModeloExistente.setEstado(0L);
        return alumnoRepositorio.save(alumnoModeloExistente);
    }

}
