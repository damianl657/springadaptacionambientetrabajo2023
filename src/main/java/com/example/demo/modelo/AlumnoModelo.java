package com.example.demo.modelo;

import javax.persistence.*;

@Entity
@Table(name="alumnos")
public class AlumnoModelo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    private String nombre;
    private String apellido;
    private String email;
    private String matricula;
    private String dni;
    private String genero;
    private Long estado;
    @ManyToOne
    @JoinColumn(name = "division_id")
    private DivisionModelo division;

    @ManyToOne
    @JoinColumn(name = "division_modelo_id")
    private DivisionModelo divisionModelo;

    public DivisionModelo getDivision() {
        return division;
    }

    public void setDivision(DivisionModelo division) {
        this.division = division;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public DivisionModelo getDivisionModelo() {
        return divisionModelo;
    }

    public void setDivisionModelo(DivisionModelo divisionModelo) {
        this.divisionModelo = divisionModelo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Long getEstado() {
        return estado;
    }

    public void setEstado(Long estado) {
        this.estado = estado;
    }
}
